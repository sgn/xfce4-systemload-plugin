# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Theppitak Karoonboonyanan <thep@linux.thai.net>, 2013
# Theppitak Karoonboonyanan <theppitak@gmail.com>, 2013,2017
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-30 00:54+0100\n"
"PO-Revision-Date: 2022-01-29 23:54+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Thai (http://www.transifex.com/xfce/xfce-panel-plugins/language/th/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: th\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../panel-plugin/cpu.c:63
msgid "File /proc/stat not found!"
msgstr "ไม่พบแฟ้ม /proc/stat"

#: ../panel-plugin/systemload.c:217
#, c-format
msgid "System Load: %ld%%"
msgstr "โหลดระบบ: %ld%%"

#: ../panel-plugin/systemload.c:225
#, c-format
msgid "Memory: %ldMB of %ldMB used"
msgstr "หน่วยความจำ: ใช้ไป %ldMB จาก %ldMB"

#: ../panel-plugin/systemload.c:233
#, c-format
msgid "Network: %ld Mbit/s"
msgstr "เครือข่าย: %ld Mbit/s"

#: ../panel-plugin/systemload.c:243
#, c-format
msgid "Swap: %ldMB of %ldMB used"
msgstr "พื้นที่สลับ: ใช้ไป %ldMB จาก %ldMB"

#: ../panel-plugin/systemload.c:246
#, c-format
msgid "No swap"
msgstr "ไม่มีพื้นที่สลับ"

#: ../panel-plugin/systemload.c:261
#, c-format
msgid "%dd"
msgstr "%dว"

#: ../panel-plugin/systemload.c:262
#, c-format
msgid "%dh"
msgstr "%dช"

#: ../panel-plugin/systemload.c:263
#, c-format
msgid "%dm"
msgstr "%dน"

#: ../panel-plugin/systemload.c:265
#, c-format
msgid "%d day"
msgid_plural "%d days"
msgstr[0] "%d วัน"

#: ../panel-plugin/systemload.c:266
#, c-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] "%d ชั่วโมง"

#: ../panel-plugin/systemload.c:267
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d นาที"

#: ../panel-plugin/systemload.c:274
#, c-format
msgid "Uptime: %s, %s, %s"
msgstr "Uptime: %s, %s, %s"

#: ../panel-plugin/systemload.c:750
msgid "Label:"
msgstr "ฉลาก:"

#: ../panel-plugin/systemload.c:759
msgid "Leave empty to disable the label"
msgstr "ปล่อยว่างไว้หากไม่ต้องการแสดงฉลาก"

#: ../panel-plugin/systemload.c:800
msgid "CPU monitor"
msgstr "เฝ้าสังเกตซีพียู"

#: ../panel-plugin/systemload.c:801
msgid "Memory monitor"
msgstr "เฝ้าสังเกตหน่วยความจำ"

#: ../panel-plugin/systemload.c:802
msgid "Network monitor"
msgstr "เฝ้าสังเกตเครือข่าย"

#: ../panel-plugin/systemload.c:803
msgid "Swap monitor"
msgstr "เฝ้าสังเกตพื้นที่สลับ"

#: ../panel-plugin/systemload.c:804
msgid "Uptime monitor"
msgstr "เฝ้าสังเกต uptime"

#: ../panel-plugin/systemload.c:815 ../panel-plugin/systemload.desktop.in.h:1
msgid "System Load Monitor"
msgstr "เครื่องมือเฝ้าสังเกตโหลดระบบ"

#: ../panel-plugin/systemload.c:818
msgid "_Close"
msgstr "ปิ_ด"

#: ../panel-plugin/systemload.c:819
msgid "_Help"
msgstr "_วิธีใช้"

#: ../panel-plugin/systemload.c:837
msgid "<b>General</b>"
msgstr "<b>ทั่วไป</b>"

#: ../panel-plugin/systemload.c:856
msgid "Update interval:"
msgstr "คาบการปรับข้อมูล:"

#: ../panel-plugin/systemload.c:862
msgid ""
"Update interval when running on battery (uses regular update interval if set"
" to zero)"
msgstr "คาบการปรับข้อมูลขณะใช้ไฟฟ้าจากแบตเตอรี่ (จะใช้ค่าเดียวกับคาบการปรับข้อมูลปกติถ้ากำหนดเป็นศูนย์)"

#: ../panel-plugin/systemload.c:873
msgid "Power-saving interval:"
msgstr "คาบขณะประหยัดพลังงาน:"

#: ../panel-plugin/systemload.c:880
msgid "Launched when clicking on the plugin"
msgstr "จะเรียกทำงานเมื่อคลิกที่ปลั๊กอิน"

#: ../panel-plugin/systemload.c:887
msgid "System monitor:"
msgstr "เครื่องมือเฝ้าสังเกตระบบ:"

#: ../panel-plugin/systemload.c:925 ../panel-plugin/systemload.desktop.in.h:2
msgid "Monitor CPU load, swap usage and memory footprint"
msgstr "เฝ้าสังเกตโหลดของซีพียู, การใช้พื้นที่สลับ และการใช้หน่วยความจำ"

#: ../panel-plugin/systemload.c:927
msgid "Copyright (c) 2003-2022\n"
msgstr ""

#: ../panel-plugin/uptime.c:52
msgid "File /proc/uptime not found!"
msgstr "ไม่พบแฟ้ม /proc/uptime"
